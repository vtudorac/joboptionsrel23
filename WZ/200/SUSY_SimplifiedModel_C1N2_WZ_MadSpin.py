### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)
#------------------------

# C1/N2 denegenerate
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
joName = get_physics_short()
jobConfigParts = joName.split("_")
print("jobConfigParts",jobConfigParts )
  
dM=StringToFloat(jobConfigParts[4])-StringToFloat(jobConfigParts[5])
masses['1000023'] = StringToFloat(jobConfigParts[4])
masses['1000024'] = StringToFloat(jobConfigParts[4])
masses['1000022'] = StringToFloat(jobConfigParts[5])
print("masses",masses['1000023'] ,masses['1000024'] ,masses['1000022'] )
print(jobConfigParts[0])


madspindecays=False
if "MadSpin" in jobConfigParts[0] and dM<100.:
  madspindecays=True

# Set Gen and Decay types 
gentype = str(jobConfigParts[2])
decaytype = str(jobConfigParts[3])
print("gentype",gentype)
print("decaytype",decaytype)

#----------------------------
# MadGraph options


decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
    
  #
  """
decays['1000024'] = """DECAY   1000024     2.10722495E-01   # chargino1 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_30 -> ~chi_10   W )
  #
  """

process='''
define c1 = x1+ x1-
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g
define sleptons = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ h+ h- svt svm sve svt~ svm~ sve~
'''

njets=2
# 
#--------------------------------------------------------------

#--------------------------------------------------------------
# Configure the process definitions.
#
mgprocstring=""
mgdecaystring=""
msdecaystring=""
if gentype=="C1N2":
  mgprocstring="p p > c1 n2"
  mergeproc="{x1+,1000024}{x1-,-1000024}{n2,1000023}"
  if madspindecays:
     raise RuntimeError("MadSpin decays not possible for N2C1+ and N2C1- in the same job!")
     
elif gentype=="C1pN2":
  mgprocstring="p p > x1+ n2"
  mergeproc = '{x1+,1000024}{n2,1000023}'
  
  
  
elif gentype=="C1mN2":
  mgprocstring="p p > x1- n2"
  mergeproc = '{x1-,-1000024}{n2,1000023}'

else:
  raise RuntimeError("Unknown process %s, aborting." % gentype)


# write out the actual processes.
processCounter = 0
for i in range(njets+1):      
  
  process += "%-12s %s %-10s %s  / susystrong @%d\n" % ('generate' if processCounter==0 else 'add process', 
                                                          mgprocstring,
                                                          ' j'*i,
                                                          mgdecaystring,
                                                          processCounter+1)
  processCounter = processCounter + 1


print("process",process)
# print the process, just to confirm we got everything right
print ("Final process card:")
print (process)

# change this back, to use a single parameter card for the generation
gentype = "C1N2"
#
#--------------------------------------------------------------

#--------------------------------------------------------------
# Madspin configuration
#
if madspindecays==True:
  if msdecaystring=="":
    raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
  madspin_card='madspin_card_C1N2_WZ.dat'
  mscard = open(madspin_card,'w')  

  mscard.write("""#************************************************************
#*                        MadSpin                           *                
#*                                                          *                
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                
#*                                                          *                
#*    Part of the MadGraph5_aMC@NLO Framework:              *                
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                
#*                                                          *                
#************************************************************                
#Some options (uncomment to apply)                                           
#                                                                            
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut 1000              # cut on how far the particle can be off-shell         
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event   
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s

# running the actual code                        
launch"""%(runArgs.randomSeed,msdecaystring))                    
  mscard.close()
  mergeproc+="LEPTONS,NEUTRINOS"


#
#--------------------------------------------------------------

evgenLog.info('Registered generation of ~chi1+/- ~chi20 production, decay via WZ; grid point '+str(jobConfigParts)+' decoded into mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))
evgenConfig.contact  = [ "Valentina.Tudorache@cern.ch"]
evgenConfig.keywords += ['gaugino', 'chargino', 'neutralino']
evgenConfig.description = '~chi1+/- ~chi20 production, decay via WZ in simplified model, m_C1N2 = %s GeV, m_N1 = %s GeV'%(masses['1000024'],masses['1000022'])



#--------------------------------------------------------------
#Filter and event multiplier 
evt_multiplier = 30

if '1L' in jobConfigParts:
    evgenLog.info('1L3 filter is applied')

    if dM==10.: evt_multiplier = 150
    
    
    include("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    filtSeq.xAODMultiElecMuTauFilter.NLeptons = 1
    filtSeq.xAODMultiElecMuTauFilter.MinPt = 3000.
    filtSeq.xAODMultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.xAODMultiElecMuTauFilter.IncludeHadTaus = 0
    filtSeq.Expression = "xAODMultiElecMuTauFilter"
    
elif '1L202L3' in jobConfigParts:
    evgenLog.info('1Lepton20 and 2Lepton3 filter is applied')

    if dM==10.: evt_multiplier = 150
    
    
    include("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    filtSeq += xAODMultiElecMuTauFilter("DiLeptonFilter")
    filtSeq += xAODMultiElecMuTauFilter("SingleLeptonFilter")
    
    xAODMultiElecMuTauFilter2 = filtSeq.DiLeptonFilter
    xAODMultiElecMuTauFilter2.NLeptons = 2
    xAODMultiElecMuTauFilter2.MinPt = 3000.
    xAODMultiElecMuTauFilter2.MaxEta = 2.8
    xAODMultiElecMuTauFilter2.IncludeHadTaus = 0

    xAODMultiElecMuTauFilter1 = filtSeq.SingleLeptonFilter
    xAODMultiElecMuTauFilter1.NLeptons = 1
    xAODMultiElecMuTauFilter1.MinPt = 20000.
    xAODMultiElecMuTauFilter1.MaxEta = 2.8
    xAODMultiElecMuTauFilter1.IncludeHadTaus = 0
    
    filtSeq.Expression = "SingleLeptonFilter and DiLeptonFilter"


elif '1L20orXe' in jobConfigParts:

    if dM==10.: evt_multiplier = 150
    
    evgenLog.info('ControlFile: 1Lep OR Xe filter is applied.')

    include ("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
    from GeneratorFilters.GeneratorFiltersConf import xAODMultiElecMuTauFilter
    f1L3Filter = xAODMultiElecMuTauFilter("f1L3Filter")
    filtSeq += f1L3Filter
    filtSeq.f1L3Filter.NLeptons = 1
    filtSeq.f1L3Filter.MinPt = 3000.0
    filtSeq.f1L3Filter.MaxEta = 2.8
    filtSeq.f1L3Filter.MinVisPtHadTau = 3000.0
    filtSeq.f1L3Filter.IncludeHadTaus = False
    
    f1L20Filter = xAODMultiElecMuTauFilter("f1L20Filter")
    filtSeq += f1L20Filter
    filtSeq.f1L20Filter.NLeptons = 1
    filtSeq.f1L20Filter.MinPt = 20000.0
    filtSeq.f1L20Filter.MaxEta = 2.8
    filtSeq.f1L20Filter.MinVisPtHadTau = 20000.0
    filtSeq.f1L20Filter.IncludeHadTaus = False

  
    include ("GeneratorFilters/xAODMETFilter_Common.py")
    filtSeq.xAODMissingEtFilter.METCut = 100000.0
    filtSeq.xAODMissingEtFilter.UseNeutrinosFromHadrons = False

    filtSeq.Expression = "f1L3Filter and (f1L20Filter or xAODMissingEtFilter)"
    


#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Pythia options
#
evgenLog.info('Using Merging:Process = guess')
genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 
